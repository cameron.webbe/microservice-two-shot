import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './Shoes/ShoeList';
import HatList from './Hats/HatsList';
import ShoeForm from './Shoes/ShoeForm';
import CreateHat from './Hats/HatsCreate';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoeList />} />
          <Route path="/hats" element={<HatList />} />
          <Route path="/shoes/new" element={<ShoeForm />} />
          <Route path="/hats/new" element={<CreateHat />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
