import React from "react";

class ShoeForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      manufacturer: "",
      modelName: "",
      color: "",
      url: "",
      bin: "",
      bins: []
    }

  this.handleModelNameChange = this.handleModelNameChange.bind(this);
  this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
  this.handleColorChange = this.handleColorChange.bind(this);
  this.handleURLChange = this.handleURLChange.bind(this);
  this.handleBinChange = this.handleBinChange.bind(this);
  this.handleSubmit = this.handleSubmit.bind(this);

  };

  async componentDidMount() {
    const url = 'http://localhost:8100/api/bins/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({bins: data.bins})
    }
  }

  handleModelNameChange(e) {
    const value = e.target.value;
    this.setState({modelName: value})
  }

  handleManufacturerChange(e) {
    const value = e.target.value;
    this.setState({manufacturer: value})
  }

  handleColorChange(e) {
    const value = e.target.value;
    this.setState({color: value})
  }

  handleURLChange(e) {
    const value = e.target.value;
    this.setState({url: value})
  }

  handleBinChange(e) {
    const value = e.target.value;
    console.log(value);
    this.setState({bin: value})
  }

  async handleSubmit(e) {
    e.preventDefault();

    const data = {...this.state};
    data.model_name = data.modelName;
    delete data.modelName;
    delete data.bins;

    const shoeURL = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      }
    };

    const response = await fetch(shoeURL, fetchConfig);
    if (response.ok) {
      const newShoe = await response.json();
      console.log(newShoe);

      const cleared = {
        manufacturer: "",
        modelName: "",
        color: "",
        url: "",
        bin: "",
      }

      this.setState(cleared)
    }
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Log a new pair of shoes</h1>
              <form id="create-shoe-form" onSubmit={this.handleSubmit}>
                <div className="form-floating mb-3">
                  <input 
                    placeholder="Model Name"
                    required 
                    type="text" 
                    id="modelName" 
                    name="modelName" 
                    className="form-control"
                    onChange={this.handleModelNameChange}
                    value={this.state.modelName}
                  />
                <label htmlFor="name">Model Name</label>
                </div>
                <div className="form-floating mb-3">
                <input 
                    placeholder="Model Name"
                    required 
                    type="text" 
                    id="manufacturer" 
                    name="manufacturer" 
                    className="form-control"
                    onChange={this.handleManufacturerChange}
                    value={this.state.manufacturer}
                  />
                  <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                <input 
                    placeholder="Color"
                    required 
                    type="text" 
                    id="color" 
                    name="color" 
                    className="form-control"
                    onChange={this.handleColorChange}
                    value={this.state.color}
                  />
                  <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                <input 
                    placeholder="Picture Url"
                    required 
                    type="url" 
                    id="url" 
                    name="url" 
                    className="form-control"
                    onChange={this.handleURLChange}
                    value={this.state.url}
                  />
                  <label htmlFor="color">Picture Url</label>
                </div>
                <div className="mb-3">
                  <select 
                    required 
                    id="bin" 
                    name="bin"
                    className="form-select"
                    onChange={this.handleBinChange}
                    value={this.state.bin}
                  >
                    <option value="">Choose a bin</option>
                    {
                      this.state.bins.map(bin => (
                        <option key={bin.href} value={bin.href}>
                          {bin.closet_name}
                        </option>
                      ))
                    }
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ShoeForm;