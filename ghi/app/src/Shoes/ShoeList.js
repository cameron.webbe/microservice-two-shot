import React from 'react';
import { Link } from 'react-router-dom';


class ShoeList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      shoes: [],
    }
  };

  async componentDidMount() {
    const url = 'http://localhost:8080/api/shoes/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log("HEREEEE", data);
      this.setState({shoes: data.shoes})
    }
  }

  async deleteClick(shoe) {
    const isConfirmed = window.confirm(`Are you sure you want to delete this pair of ${shoe.model_name}s?`);
    if (isConfirmed) {
      const deleteRespose = await fetch(`http://localhost:8080/api/shoes/${shoe.id}`, {method: 'delete'})
      if (deleteRespose.ok) {
        const oldState = this.state.shoes;
        this.setState({shoes: oldState.filter(shoeObj => shoeObj.id !== shoe.id)})
        console.log('delete')
      }
    }
  }
  
  render () {
    return (
      <>
        <div className="">
          <h2 className="text-center p-3">Shoes in your wardrobe</h2>
          <div className="text-center">
            <Link to="/shoes/new" className="btn btn-primary mb-3">
              Add New Pair
            </Link>
          </div>
          <div className="d-flex flex-wrap">
            {this.state.shoes.map((shoe) => ( 
              <div key={shoe.id} className="card m-3 shadow">
                <div className="card-body">
                  <div className="text-center">
                    <img
                      height={250}
                      width={250}
                      src={shoe.url}
                      className=""
                      alt="Shoe product display"
                    />
                  </div>
                  <div className="d-flex justify-content-between">
                    <div>
                      <h5 className="card-title">
                        {shoe.model_name}
                      </h5>
                      <h6 className="card-subtitle mb-2 text-muted">
                        {shoe.manufacturer}
                        <br />
                        Color: {shoe.color}
                      </h6>
                    </div>
                    <button
                      className="btn btn-danger align-self-center"
                      onClick={() =>
                        this.deleteClick(shoe)
                      }
                    >
                      Delete
                    </button>
                  </div>
                </div>
                <div className="card-footer">
                  {<p>Located in: {shoe.bin.name}</p>}
                </div>
              </div>
            ))}
          </div>
        </div>
      </>
    );
  }
}

export default ShoeList;