import json
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Hats, LocationVO
# Create your views here.

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "name",
        "import_href"
    ]

class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "style_name",
        "color",
        "fabric",
        "url",
        "location",
        "id",
    ]
    encoders = {
        "location": LocationVOEncoder()
    }

class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "style_name",
        "color",
        "fabric",
        "url",
        "location",
        "id",
    ]
    encoders = {
        "location": LocationVOEncoder()
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Location Invalid"},
                status=400
            )
        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsListEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET"])
def api_show_hats(request, pk):
    if request.method == "GET":
        hat = Hats.objects.filter(id=pk)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

