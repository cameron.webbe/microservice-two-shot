from django.db import models
# Create your models here.

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)

class Hats(models.Model):
    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    url = models.URLField(max_length=200)
    location = models.ForeignKey(LocationVO, related_name="hats", on_delete=models.CASCADE)
    
