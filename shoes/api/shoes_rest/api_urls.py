from django.urls import path

from .api_views import api_list_shoes, api_show_shoe

urlpatterns = [
    path("shoes/", api_list_shoes, name="api_shoes_list"),
    path("shoes/<int:pk>", api_show_shoe, name="api_shoe_detail")
]
