import json

from django.views.decorators.http import require_http_methods
from django.http import JsonResponse

from common.json import ModelEncoder
from .models import Shoes, BinVO


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "name",
        "import_href"
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "url",
        "bin",
    ]
    encoders = {
        "bin": BinVODetailEncoder()
    }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "url",
        "bin",
    ]
    encoders = {
        "bin": BinVODetailEncoder()
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin href"},
                status=400
            )
        shoe = Shoes.objects.create(**content)
        return JsonResponse(
            shoe,
            safe=False,
            encoder=ShoeListEncoder
        )


@require_http_methods(["DELETE", "GET"])
def api_show_shoe(request, pk):
    try:
        if request.method == "GET":
            shoe = Shoes.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False
            )
    except Shoes.DoesNotExist:
        return JsonResponse(
            {"message": f"Shoe with id={pk} not found."},
            status=404
        )
    else:
        try:
            count, _ = Shoes.objects.get(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Shoes.DoesNotExist:
            return JsonResponse(
                {"message": f"Shoe with id={pk} not found."},
                status=404
            )
